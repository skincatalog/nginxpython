# nginxpython

This is a docker image based python 3.8. it uses the alpine python image and adds nginx with gunicorn.  

It is expected to be used as a base image in your poetry/django projects.  

To launch,   

 docker run -it -p 80:8080 \
     -e WSGI_SCRIPT=wsgi \
     registry.gitlab.com/skincatalog/nginxpython:latest

