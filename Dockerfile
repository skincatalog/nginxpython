FROM python:3.8-alpine

ENV POETRY_HOME /var/www/webroot/.poetry

RUN apk add --no-cache shadow \
                       nginx \
                       postgresql-dev \
		       build-base \
		       python3-dev \
		       musl-dev \
               libffi-dev \
                       jpeg-dev \
		       zlib-dev \
 && usermod -s /bin/sh nginx \
 && pip install virtualenv

RUN mkdir -p /var/run/nginx && chown nginx:nginx /var/run/nginx \
 && mkdir -p /var/log/nginx && chown nginx:nginx /var/log/nginx \
 && mkdir -p /var/www/webroot/ROOT \
 && mkdir -p /var/www/webroot/public \
 && mkdir -p /var/www/webroot/public/images \
# && ln -sf /dev/stdout /var/log/nginx/access.log \
# && ln -sf /dev/stderr /var/log/nginx/error.log \
&& ln -s /var/lib/nginx/virtenv /var/www/webroot/virtenv

COPY website_config/nginx.conf /etc/nginx/nginx.conf

WORKDIR /var/www/webroot

COPY app/pyproject.toml ROOT/pyproject.toml
COPY app/wsgi.py ROOT/wsgi.py
COPY app/pyinfo.py ROOT/pyinfo.py
COPY get-poetry.py ./

COPY website_config/start-server.sh ./
RUN chmod +x start-server.sh

COPY website_config/start-server-jel.sh ./
RUN chmod +x start-server-jel.sh

RUN chown -R nginx:nginx ./
USER nginx

COPY website_config/50x.html /var/lib/nginx/html/50x.html
# cause ~/virtenv = /var/lib/nginx/virtenv
WORKDIR /var/lib/nginx
RUN virtualenv virtenv

WORKDIR /var/www/webroot/ROOT

RUN . /var/www/webroot/virtenv/bin/activate && \
    python ../get-poetry.py && \
    . /var/www/webroot/.poetry/env && \
    poetry config virtualenvs.create false && \
    poetry env info && \
    poetry install

#VOLUME /var/www/webroot/ROOT
VOLUME /var/www/webroot/public/images

ENV WSGI_SCRIPT wsgi
EXPOSE 8080
STOPSIGNAL SIGTERM
CMD ["/var/www/webroot/start-server.sh"]
