# nginx.default
#user                            www;
# pid                             /run/nginx/nginx.pid; # it permit you to use /etc/init.d/nginx reload|restart|stop|start

worker_processes                auto; # it will be determinate automatically by the number of core

error_log                       /var/log/nginx/error.log warn;

events {
    worker_connections          1024;
}
http {
# jelastic upstream settings
#                        proxy_set_header X-Real-IP $remote_addr;
#                        proxy_set_header X-Host $http_host;
#                        proxy_set_header X-Forwarded-For $http_x_forwarded_for;
#                        proxy_set_header X-URI $uri;
#                        proxy_set_header X-ARGS $args;
#                        proxy_set_header Refer $http_refer;
#                        proxy_set_header X-Forwarded-Proto $scheme;

    log_format  main  '$remote_addr:$http_x_remote_port - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for" '
                      '"$host" sn="$server_name" '
                      'rt=$request_time '
                      'ua="$upstream_addr" us="$upstream_status" '
                      'ut="$upstream_response_time" ul="$upstream_response_length" '
                      'cs=$upstream_cache_status' ;

    include                     /etc/nginx/mime.types;
    default_type                application/octet-stream;
    sendfile                    on;
    access_log                  /var/log/nginx/access.log main;
    keepalive_timeout           3000;


    upstream app_server {
        # fail_timeout=0 means we always retry an upstream even if it failed
        # to return a good HTTP response

        # for UNIX domain socket setups
        # server unix:/tmp/gunicorn.sock fail_timeout=0;

        # for a TCP configuration
        server 127.0.0.1:8010 fail_timeout=0;
    }

    server {
        listen 8080;
        server_name _;

        client_max_body_size 4G;

        keepalive_timeout 5;

        # path for static files
        root /var/www/webroot/public;

        location / {
          # checks for static file, if not found proxy to app
          try_files $uri @proxy_to_app;
        }

        location @proxy_to_app {
          proxy_set_header X-Forwarded-For $http_x_forwarded_for;
          proxy_set_header X-Forwarded-Proto $scheme;
          proxy_set_header Host $http_host;
          # we don't want nginx trying to do something clever with
          # redirects, we set the Host: header above already.
          proxy_redirect off;
          proxy_pass http://app_server;
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
                root /var/lib/nginx/html;
                internal;
                ssi on;
        }
    }

    map $status $status_text {
    400 'Bad Request';
    401 'Unauthorized';
    402 'Payment Required';
    403 'Forbidden';
    404 'Not Found';
    405 'Method Not Allowed';
    406 'Not Acceptable';
    407 'Proxy Authentication Required';
    408 'Request Timeout';
    409 'Conflict';
    410 'Gone';
    411 'Length Required';
    412 'Precondition Failed';
    413 'Payload Too Large';
    414 'URI Too Long';
    415 'Unsupported Media Type';
    416 'Range Not Satisfiable';
    417 'Expectation Failed';
    418 'I\'m a teapot';
    421 'Misdirected Request';
    422 'Unprocessable Entity';
    423 'Locked';
    424 'Failed Dependency';
    425 'Too Early';
    426 'Upgrade Required';
    428 'Precondition Required';
    429 'Too Many Requests';
    431 'Request Header Fields Too Large';
    451 'Unavailable For Legal Reasons';
    500 'Internal Server Error';
    501 'Not Implemented';
    502 'Bad Gateway';
    503 'Service Unavailable';
    504 'Gateway Timeout';
    505 'HTTP Version Not Supported';
    506 'Variant Also Negotiates';
    507 'Insufficient Storage';
    508 'Loop Detected';
    510 'Not Extended';
    511 'Network Authentication Required';
    default 'Unexpected Error <b>:(</b>';
  }
}
