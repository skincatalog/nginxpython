#!/bin/sh
# start-server.sh
# docker run -it -p 8020:8020 \
#     -e DJANGO_SUPERUSER_USERNAME=admin \
#     -e DJANGO_SUPERUSER_PASSWORD=sekret1 \
#     -e DJANGO_SUPERUSER_EMAIL=admin@example.com \
#     -e SECRET_KEY=busdlkjj45l3k
#     -e DJANGO_SETTINGS_MODULE=skincatalog.settings.development
#     -e DATABASE_URL=sqlite:///db.sqlite3
#     -e ALLOWED_HOSTS=localhost,127.0.0.1
#     -e WSGI_SCRIPT=skincatalog.wsgi
#     registry.gitlab.com/skincatalog/skincatalog:latest
#     image:latest
cd /var/www/webroot/ROOT
. /var/www/webroot/.poetry/env
. /var/lib/nginx/virtenv/bin/activate
poetry config virtualenvs.create false

# update run django statup tasks. migrate, staticfiles etc
if [ -f /var/www/webroot/deploy-post.sh ]; then
  sh /var/www/webroot/deploy-post.sh
fi

(poetry run gunicorn "${WSGI_SCRIPT}:application" --bind 0.0.0.0:8010 --workers 2) &
nginx -g "daemon off; pid /run/apache2/nginx.pid;"
